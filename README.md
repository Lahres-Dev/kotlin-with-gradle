[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=info.lahres.template.kotlingradle%3Akotlin-with-gradle-template&metric=alert_status)](https://sonarcloud.io/dashboard?id=info.lahres.template.kotlingradle%3Akotlin-with-gradle-template)
[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)

## Introduction
This repository serves as template for kotlin projects which use gradle as build tool. Features are:

 * Sample kotlin class file with simple output.
 * (TODO)Multi module gradle project
 * Static code analysis with Sonar -> [Project Page](https://sonarcloud.io/dashboard?id=info.lahres.template.kotlingradle%3Akotlin-with-gradle-template)
 * Self-contained jar file creation
 * Unit tests. Results, created by JaCoCo, are found on sonar project page.
 * Sample license and readme file
 * Git as version control system with .gitignore file to exclude automatically created files from repository.

---

## Building the project
Just run the following gradle task
<pre>
build
</pre>
The output is found inside the build folder.

---

## Generating Sonar report
To generate a sonar report, the following gradle task has to be run
<pre>
sonarqube -Dsonar.login=$SONAR_LOGIN_TOKEN
</pre>
The sonar login token can be generated on the sonar account page (needs admin rights).

---

## Running the self-contained jar file
To run the self-contained jar file
<pre>
java -jar build/libs/kotlin-with-gradle-template-1.0-SNAPSHOT.jar
</pre>
Output should be:
<pre>
Hello Jack. You're 22 years old and you're gender is male.
</pre>

---
